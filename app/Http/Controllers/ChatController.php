<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Message;
use App\Events\MessagePosted;

class ChatController extends Controller
{
    public function index()
    {
        return view('layouts.chat');
    }

    public function getUserLogin()
    {
        $user = Auth::user();
        return $user;
    }

    public function getMessages()
    {
        $messages = Message::with('user')->get();
        return $messages;
    }

    public function postMessages(Request $request)
    {
        $user = Auth::user();

        $message = Message::create([
            'message' => $request->message,
            'user_id' => $user->id
        ]);
        broadcast(new MessagePosted($message, $user))->toOthers(); // May be not working
        // Redis::publish('chatroom', json_encode($message));
        return ['message' => $message->load('user')];
    }
}
